export default class Vector {
  x;
  y;
  constructor(x = null, y = null) {
    this.x = x;
    this.y = y;
  }
  add(v) {
    this.x += v.x;
    this.y += v.y;
  }
  diff(v) {
    return new Vector(this.x - v.x, this.y - v.y);
  }
  equal(v) {
    return Math.abs(x1 - x2) <= 0 && Math.abs(y1 - y2) <= 0;
  }
  update(xUp, yUp) {
    this.x = xUp;
    this.y = yUp;
  }
  copy() {
    return new Vector(this.x, this.y);
  }
  null() {
    this.x = null;
    this.y = null;
  }
  get x() {
    return this.x;
  }
  get y() {
    return this.y;
  }
  list() {
    return [this.x, this.y];
  }
  newAddX(addX) {
    return new Vector(this.x + addX, this.y);
  }
  newAddY(addY) {
    return new Vector(this.x, this.y + addY);
  }
  newAdd(v) {
    return new Vector(this.x + v.x, this.y + v.y);
  }
  dist(v) {
    return Math.sqrt((this.x - v.x) ** 2 + (this.y - v.y) ** 2);
  }
  close(v) {
    return this.dist(v) < 10 ? true : false;
  }
  set(v) {
    this.x = v.x;
    this.y = v.x;
  }
  minus(v) {
    this.x -= v.x;
    this.y -= v.y;
  }
  max() {
    return this.x < this.y ? this.y : this.x;
  }
}
