import Vector from "./vector.js";
import Piece from "./piece.js";
import winner from "./winner.js";
const FLAT = 0;
const UP = 0;
const DOWN = 1;
const LEFT = 2;
const RIGHT = 3;
var gedaan=false;
var audio = new Audio('rocky.mp3');

class Shape {
  top;
  bottom;
  left;
  right;
}
class connection {
  up;
  down;
  left;
  right;
}

export default class Puzzel {
  pieces = new Array();
  img;
  dim;
  rows;
  coloms;
  canvas;
  context;
  gedaan=false;
  margin = 110;
  colom;
  imgRatio;
  winQuote;

  constructor(dim, rs, cs, canvas, img, r, win) {
    this.img = img;
    this.dim = dim;
    this.rows = rs;
    this.coloms = cs;
    this.canvas = canvas;
    this.context = canvas.getContext("2d");
    var pDim = new Vector(dim.x / rs, dim.y / cs);
    this.margin = pDim.max() + 5;
    this.imgRatio = r;
    this.winQuote = win;
    // console.log(img.height + " : " + img.width);

    for (let i = 0; i < this.coloms; i++) {
      this.colom = [];
      for (let j = 0; j < this.rows; j++) {
        var shape = this.generateShape(i, j);
        var ctx = canvas.getContext("2d");
        var cord = this.randomCord();
        var conn = new connection();
        var stuk = new Piece(cord, i, j, shape, conn, pDim); // to do: shape, img, width, height
        this.colom.push(stuk);
      }
      this.pieces.push(this.colom);
    }

    this.drawAll();
  }
  drawAll() {
    if (gedaan==false){this.context.clearRect(0, 0, canvas.width, canvas.height);
      for (let i = 0; i < this.coloms; i++) {
        for (let j = 0; j < this.rows; j++) {
          var p = this.pieces[i][j];
          this.context.save();
          p.draw(this.context, this.imgRatio, this.img);
          this.context.restore();
        }
      }}
  }
  randomCord() {
    console.log(this.margin);
    return new Vector(
      randIntIncl(20, canvas.width - this.margin),
      randIntIncl(20, canvas.height - this.margin)
    );
  }
  clickedPiece(click) {
    for (let i = 0; i < this.coloms; i++) {
      for (let j = 0; j < this.rows; j++) {
        if (this.pieces[i][j].hitPiece(click)) {
          return new Vector(i, j);
        }
      }
    }
    return new Vector();
  }
  movePiece(diff, i, j) {
    this.getListConnected(i, j).forEach((p) => {
      p.move(diff);
    });
    this.drawAll();
  }
  generateShape(i, j) {
    var shape = new Shape();
    // up
    if (i === 0) {
      shape.top = FLAT;
    } else {
      var above = this.pieces[i - 1][j];
      shape.top = -above.shape.bottom;
    }
    // down
    if (i === this.coloms - 1) {
      shape.bottom = FLAT;
    } else {
      shape.bottom = this.randomSide();
    }
    // left
    if (j === 0) {
      shape.left = FLAT;
    } else {
      var leftOf = this.colom[j - 1];
      console.log(leftOf);
      shape.left = -leftOf.shape.right;
    }
    // right
    if (j === this.rows - 1) {
      shape.right = FLAT;
    } else {
      shape.right = this.randomSide();
    }
    return shape;
  }
  randomSide() {
    return Math.round(Math.random()) * 2 - 1;
  }
  connection(i, j) {
    var diff = null;
    // up
    if (i != 0) {
      diff = this.pieces[i][j].connect(this.pieces[i - 1][j], UP);
      if (diff != null) {
        this.getListConnected(i, j).forEach((p) => {
          p.moveb(diff);
          this.pieces[i][j].connection.up = true;
          this.pieces[i - 1][j].connection.down = true;
        });
      }
    }
    // down
    if (i != this.coloms - 1) {
      diff = this.pieces[i][j].connect(this.pieces[i + 1][j], DOWN);
      if (diff != null) {
        this.getListConnected(i, j).forEach((p) => {
          p.moveb(diff);
          this.pieces[i][j].connection.down = true;
          this.pieces[i + 1][j].connection.up = true;
        });
      }
    }
    // left
    if (j != 0) {
      diff = this.pieces[i][j].connect(this.pieces[i][j - 1], LEFT);
      if (diff != null) {
        this.getListConnected(i, j).forEach((p) => {
          p.moveb(diff);
        });
        this.pieces[i][j].connection.left = true;
        this.pieces[i][j - 1].connection.right = true;
      }
    }
    // right
    if (j != this.rows - 1) {
      diff = this.pieces[i][j].connect(this.pieces[i][j + 1], RIGHT);
      if (diff != null) {
        this.getListConnected(i, j).forEach((p) => {
          p.moveb(diff);
        });
        this.pieces[i][j].connection.right = true;
        this.pieces[i][j + 1].connection.left = true;
      }
    }

    this.drawAll();
    if (this.isComplete()) {
      gedaan=true;
      audio.play();
      winner(canvas);
      // this.scrabble();
    }
  }
  getListConnected(i, j) {
    var connList = [];
    console.log(i + " : " + j);
    connList.push(this.pieces[i][j]);
    for (var i = 0; i < connList.length; i++) {
      let c = connList[i];
      if (
        c.connection.up &&
        !connList.includes(this.pieces[c.col - 1][c.row])
      ) {
        connList.push(this.pieces[c.col - 1][c.row]);
      }
      if (
        c.connection.down &&
        !connList.includes(this.pieces[c.col + 1][c.row])
      ) {
        connList.push(this.pieces[c.col + 1][c.row]);
      }
      if (
        c.connection.left &&
        !connList.includes(this.pieces[c.col][c.row - 1])
      ) {
        connList.push(this.pieces[c.col][c.row - 1]);
      }
      if (
        c.connection.right &&
        !connList.includes(this.pieces[c.col][c.row + 1])
      ) {
        connList.push(this.pieces[c.col][c.row + 1]);
      }
    }
    return connList;
  }
  isComplete() {
    return this.getListConnected(0, 0).length < this.rows * this.coloms
      ? false
      : true;
  }
  scrabble() {
    for (let i = 0; i < this.coloms; i++) {
      for (let j = 0; j < this.rows; j++) {
        var p = this.pieces[i][j];
        p.connection.up = false;
        p.connection.down = false;
        p.connection.left = false;
        p.connection.right = false;
        p.cord = this.randomCord();
      }
    }
    gedaan=false;
    audio.pause();
    this.drawAll();
  }
}
function randIntIncl(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1) + min);
}
