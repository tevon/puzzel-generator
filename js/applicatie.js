import Puzzel from "./puzzel.js";
import Vector from "./vector.js";
var pid=1;  //Hier kies je welke puzzel er gekozen word uit de databank




var canvas = document.getElementById("canvas");
var src =
  "https://cdn.pixabay.com/photo/2022/05/18/17/22/leaves-7205773_960_720.jpg";
var winQuote = "you did it!";
var val;



// canvas.style.width = "width:70%"
// canvas.style.height = "height:80%"
console.log(
  "(window) height: " + window.innerHeight + " : width: " + window.innerWidth
);
canvas.width = (window.innerWidth * 6) / 10;
canvas.height = (window.innerHeight * 8) / 10;
console.log("(canvas) height: " + canvas.height + " : width: " + canvas.width);

var $canvas = $("#canvas");
var canvasOffset = $canvas.offset();
var offset = new Vector(canvasOffset.left, canvasOffset.top);
var scroll = new Vector(0, 0);

var mouse = new Vector();
var start = new Vector();
var selectedPiece = new Vector();
var puzzel;
var img = new Image();
var xhr = new XMLHttpRequest();
xhr.open("POST", "http://localhost/webproject/puzzel-generator/getpuzzel.php ", true);
xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

xhr.send("pid="+encodeURIComponent(String(pid)));

// image deel

img.onload = function () {
  var widthScale = 6 / 10;
  var heightScale = 7 / 10;
  var ratio = (canvas.width * widthScale) / img.width;
  console.log(ratio);
  console.log("(img) height: " + img.height + " : width: " + img.width);
  var newImgHeight = img.height * ratio;
  var newImgWidth = img.width * ratio;
  if (newImgHeight > (canvas.height * 8) / 10) {
    ratio = (canvas.height * heightScale) / img.height;
    newImgHeight = img.height * ratio;
    newImgWidth = img.width * ratio;
    console.log("switched");
  }

  var imgDim = new Vector(newImgWidth, newImgHeight);
  console.log(imgDim);
  puzzel = new Puzzel(imgDim, val["col"], val["row"], canvas, img, ratio, winQuote);
};
xhr.onreadystatechange = function () {
  if (xhr.readyState == 4 ) {
      var ant=xhr.responseText; 
      val=JSON.parse(ant);
      //alert(val["instructies"]);
      img.src = val["img"];
    }};


// mouse deel
function handleMouseDown(e) {
  e.preventDefault();

  start.update(parseInt(e.clientX), parseInt(e.clientY));
  scroll.update(Math.round(window.scrollX), Math.round(window.scrollY));
  start.minus(offset);
  start.add(scroll);

  console.log("startx: " + start.x + ", scrollx: " + scroll.x);
  console.log("starty: " + start.y + ", scrolly: " + scroll.y);
  selectedPiece = puzzel.clickedPiece(start);
  if (selectedPiece.x != null) {
    console.log("hit!");
  }
}
// done dragging
function handleMouseUp(e) {
  if (selectedPiece.x == null) {
    return;
  }
  e.preventDefault();
  puzzel.connection(selectedPiece.x, selectedPiece.y);
  selectedPiece.null();
}
// also done dragging
function handleMouseOut(e) {
  e.preventDefault();
  selectedPiece.null();
}
function handleMouseMove(e) {
  if (selectedPiece.x == null) {
    return;
  }
  e.preventDefault();
  mouse.update(parseInt(e.clientX), parseInt(e.clientY));
  scroll.update(Math.round(window.scrollX), Math.round(window.scrollY));
  mouse.minus(offset);
  mouse.add(scroll);

  var diff = mouse.diff(start);
  start = mouse.copy();

  puzzel.movePiece(diff, selectedPiece.x, selectedPiece.y);
}

$("#ontordenbutton").on("click", function () {
  puzzel.scrabble();
});

// listen for mouse events
// $("#canvas").mousedown(function (e) {
//   handleMouseDown(e);
// });
// $("#canvas").mousemove(function (e) {
//   handleMouseMove(e);
// });
// $("#canvas").mouseup(function (e) {
//   handleMouseUp(e);
// });
// $("#canvas").mouseout(function (e) {
//   handleMouseOut(e);
// });

// mouse event PC
canvas.addEventListener(
  "mousedown",
  function (e) {
    handleMouseDown(e);
  },
  false
);
canvas.addEventListener(
  "mouseup",
  function (e) {
    handleMouseUp(e);
  },
  false
);
canvas.addEventListener(
  "mousemove",
  function (e) {
    handleMouseMove(e);
  },
  false
);
canvas.addEventListener(
  "mouseout",
  function (e) {
    handleMouseOut(e);
  },
  false
);
// Set up touch events for mobile, etc
canvas.addEventListener(
  "touchstart",
  function (e) {
    var touch = e.touches[0];
    var mouseEvent = new MouseEvent("mousedown", {
      clientX: touch.clientX,
      clientY: touch.clientY,
    });
    canvas.dispatchEvent(mouseEvent);
  },
  false
);
canvas.addEventListener(
  "touchend",
  function (e) {
    var mouseEvent = new MouseEvent("mouseup", {});
    canvas.dispatchEvent(mouseEvent);
  },
  false
);
canvas.addEventListener(
  "touchcancel",
  function (e) {
    var mouseEvent = new MouseEvent("mouseup", {});
    canvas.dispatchEvent(mouseEvent);
  },
  false
);
canvas.addEventListener(
  "touchmove",
  function (e) {
    var touch = e.touches[0];
    var mouseEvent = new MouseEvent("mousemove", {
      clientX: touch.clientX,
      clientY: touch.clientY,
    });
    canvas.dispatchEvent(mouseEvent);
  },
  false
);




