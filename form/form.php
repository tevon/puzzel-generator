<!DOCTYPE html>
<?php

$titel = $afbeelding = $hor = $ver = $beloning = $geluid = $hint = $instr_titel = $instructies = $opslaan ="";
$titelErr = $afbeeldingErr = $horErr = $verErr = $beloningErr = $geluidErr = $hintErr = $instr_titelErr = $instructiesErr = $opslaanErr ="";

$titelErr = "Titel is verplicht";
$afbeeldingErr = "Afbeelding kiezen is verplicht";
$horErr = "Aantal stukken is verplicht";
$verErr = "Aantal stukken is verplicht";
?>
<html>
	<head>
		<title> Puzzel - WAI-NOT</title>
		<link rel='stylesheet' href='stylesheet.css'>
	</head>
	<body>
		<h1>Puzzel - WAI-NOT</h1>
		<form action="process_form.php" method="post" enctype="multipart/form-data" onsubmit="return required()" name="form1">
			<label for='titel' class='titles' id='titel'> Titel:</label>
			<div class="error" >*<?php echo $titelErr;?></div>
			<input type='text' class='error_field' id='titel_input' name='titel' value='<?php echo $titel;?>'>

			<label for='afbeelding' class='titles' id='afbeelding'> Afbeelding:</label>
			<div class="error">*<?php echo $afbeeldingErr;?></div><br>
			<input type='text' id='afbeelding_input' name='afbeelding'  ><br>

			<label for='hor' class='titles' id='hor'> Horizontale stukken:</label>
			<div class="error">*<?php echo $horErr;?></div>
			<input type='text' pattern="[0-9]+" class='error_field' id='hor_input' name='hor' value='<?php echo $hor;?>' >

			<label for='ver' class='titles' id='ver'> Verticale stukken:</label>
			<div class="error">*<?php echo $verErr;?></div><br>
			<input type='text' pattern="[0-9]+" class='error_field' id='ver_input' name='ver' value='<?php echo $ver;?>' >

			<label for='beloning' class='titles' id='beloning'> Beloningswoord:</label><br>
			<input type='text' id='beloning_input' name='beloning' value='<?php echo $beloning;?>'><br>

			<label for='geluid' class='titles' id='geluid'> Geluidsfragment beloning:</label><br>
			<input type='file' id='geluid_input' name='geluid' accept='.mp3,.mp4,.wav,.ogg,.aac'><br>

			<div>
				<label for='toon_hint' class='titles'>Toon hint:</label>
					<a href='#' class='info'> <img src='info.png' alt='info?' src='info.png' id='info_afbeelding'><br>
						<span> Sta leerlingen toe om een voorvertoning van de voltooide puzzel te zien.</span>
					</a>
				<input type='checkbox' id='hint_input' name='hint' <?php if (isset($hint)) echo "checked";?>>Toon hint<br>
			</div>

			<fieldset>
				<div>
					<label for='instructies_head' class='titles' id='instuctions'>Instructies:</label>
					<a href='#' class='info'> <img src='info.png' alt='info?' src='info.png' id='info_afbeelding'><br>
						<span> Instructies voor de leerling. Ze worden weergegeven wanneer de widget wordt gestart en kunnen met m.b.v. de info-knop worden weergegeven/verborgen.</span>
					</a><br>
				</div>
				<div>
					<label for='instr_titel' class='titles'>Titel:</label><br>
					<input type='text' id='instr_titel_input' name='instr_titel' value='Instructies'><br>
					<label for='instructies' class='titles'>Instructies:</label><br>
					<textarea name='instructies' rows='5' cols='50' value='<?php echo $instructies;?>'></textarea>
				</div>
			</fieldset>

			<fieldset>
				<div>
					<label for='opslaan_optie' class='titles' id='opslaan_optie'> Antwoorden opslaan en laden?</label>
					<a href='#' class='info'> <img src='info.png' alt='info?' src='info.png' id='info_afbeelding'><br>
						<span> Indien 'ja' kunnen studenten hun voortgang van hun puzzel opslaan en later aan verder werken</span>
					</a><br>
				</div>
				<label for='opslaan'> Ja</label>
				<input type='radio' id='opslaan_ja_input' name='opslaan'  <?php if (isset($opslaan) && $opslaan=="Ja") echo "checked";?>><br>
				<label for='opslaan' > Nee</label>
				<input type='radio' id='opslaan_nee_input' name='opslaan' <?php if (isset($opslaan) && $opslaan=="Nee") echo "checked";?>><br>
			</fieldset>

			<input type="reset" value="Reset">
			<input type="submit" value="Klaar">
		<form>
	</body>
</html>

<script>
	function required(){
		var afbeelding = document.forms["form1"]["afbeelding"].value;
		var v = document.forms["form1"]["ver"].value;
		var h = document.forms["form1"]["hor"].value;
		var titel = document.forms["form1"]["titel"].value;

		if( v=="" || h=="" || titel == "" || afbeelding == ""){
			style();
			alert("Vul alle verplichte velden in");
			return false;
		}

		else{
			return true;
		}
	}

	function style(){
		const bs = 	document.querySelectorAll('.error')
		bs.forEach(error =>{
			error.style.display='inline';
			error.style.textShadow='0.3px 0.3px grey';
		});

		const ef = document.querySelectorAll('.error_field')
		ef.forEach(error_field => {
			error_field.style.background='#fcffa4';
		});

	}
</script>
