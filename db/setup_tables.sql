CREATE TABLE IF NOT EXISTS puzzels
(
    Pid INT AUTO_INCREMENT,
    nRows INT NOT NULL,
    nColoms INT NOT NULL,
    pSave INT ,
    hint INT,
    title varchar(20),
    img varchar(400),
    instr_t varchar(20),
    instructies varchar(160),
    reward varchar(20),
    PRIMARY KEY (Pid)
);
